@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Products</h1>
        <a href="{{ route('products.create') }}" class="btn btn-primary">Create Product</a>
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Image</th> <!-- Add the image column -->
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                <tr>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->category->name }}</td>
                    <td>
                        @if($product->image)
                            <img src="{{ asset('storage/products/' . $product->image) }}" alt="Product Image">
                        @else
                            No Image
                        @endif
                    </td>
                    <td>{{ $product->status }}</td>
                    <td>
                    <a href="{{ route('products.show', $product) }}" class="btn btn-success">View</a>

                        <a href="{{ route('products.edit', $product) }}" class="btn btn-primary">Edit</a>
                        <form action="{{ route('products.destroy', $product) }}" method="POST" style="display: inline;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
