@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Product Details</h1>
        <div class="card">
            <div class="card-body">
                <p class="card-text">
                <strong>Name:</strong> {{ $product->name }}<br>
                    <strong>Category:</strong> {{ $product->category->name }}<br>
                    <strong>Image:</strong>
                    @if($product->image)
                        <img src="{{ asset('storage/products/' . $product->image) }}" alt="Product Image">
                    @else
                        No Image
                    @endif
                    <br>
                    <strong>Status:</strong> {{ $product->status }}
                </p>
            </div>
        </div>
        <br>
        <a href="{{ route('products.index') }}" class="btn btn-primary">Back to Products</a>
    </div>
@endsection
