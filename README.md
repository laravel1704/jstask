# product management

product management is a Laravel project for dealing with crud operations.

## Installation

Clone the project 

## Make sure to run the below command for image storage path

php artisan storage:link

## To run the project 

php artisan serve

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
