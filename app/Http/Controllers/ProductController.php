<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;


class ProductController extends Controller
{
    public function index()
{
    $products = Product::all();
    return view('products.index', compact('products'));
}

public function show(Product $product)
{
    return view('products.show', compact('product'));
}


public function create()
{
    $categories = Category::all();
    return view('products.create', compact('categories'));
}


public function store(Request $request)
{
    $request->validate([
        'name' => 'required',
        'category_id' => 'required',
        'status' => 'required',
        'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048', // Add image validation rules
    ]);

    $data = $request->all();

    // Handle image upload
    if ($request->hasFile('image')) {
        $image = $request->file('image');
        $imageName = time() . '.' . $image->getClientOriginalExtension();
        $image->storeAs('public/products', $imageName);
        $data['image'] = $imageName;
    }

    Product::create($data);

    return redirect()->route('products.index')
        ->with('success', 'Product created successfully.');
}

public function edit(Product $product)
{
    $categories = Category::all();
    return view('products.edit', compact('product', 'categories'));
}

public function update(Request $request, Product $product)
{
    $request->validate([
        'name' => 'required',
        'category_id' => 'required',
        'status' => 'required',
        'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048', // Add image validation rules
    ]);

    $data = $request->all();

    // Handle image update
    if ($request->hasFile('image')) {
        $image = $request->file('image');
        $imageName = time() . '.' . $image->getClientOriginalExtension();
        $image->storeAs('public/products', $imageName);
        $data['image'] = $imageName;

        // Delete the old image if it exists
        if ($product->image) {
            Storage::delete('public/products/' . $product->image);
        }
    }

    $product->update($data);

    return redirect()->route('products.index')
        ->with('success', 'Product updated successfully.');
}

public function destroy(Product $product)
{
    // Delete the associated image, if it exists
    if ($product->image) {
        Storage::delete('public/products/' . $product->image);
    }

    $product->delete();

    return redirect()->route('products.index')
        ->with('success', 'Product deleted successfully.');
}
}
